module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  testPathIgnorePatterns: ['src/'],
  testTimeout: 30000,
  moduleNameMapper: {
    "^@controllers(.*)$":"<rootDir>/dist/controllers$1",
    "^@middlewares(.*)$":"<rootDir>/dist/middlewares$1",
    "^@utils(.*)$" : "<rootDir>/dist/utils$1",
    "^@libs(.*)$" : "<rootDir>/dist/libs$1",
    "^@views(.*)$":"<rootDir>/dist/views$1",
    "^@config(.*)$":"<rootDir>/dist/config$1",
    "^@collections(.*)$":"<rootDir>/dist/collections$1",
    "^@interfaces(.*)$":"<rootDir>/dist/interfaces$1"
  }
};