module.exports = {
    apps : [{
        name: 'development',
        script: 'dist/index.js',
        watch: true,
        exec_mode: 'cluster',
        error_file: 'logs/err.log',
        out_file: 'logs/out.log',
        log_file: 'logs/app.log',
        merge_logs: true,
        log_date_format: 'YYYY-MM-DD HH:mm Z',
        instances: 1,
        max_restarts: 3,
        ignore_watch: ['node_modules', 'logs'],
        source_map_support: true,
        env: {
            NODE_ENV: 'development',
        },
    },
    {
        name: 'production',
        script: 'dist/index.js',
        max_restarts: 20,
        error_file: 'logs/err.log',
        out_file: 'logs/out.log',
        log_file: 'logs/app.log',
        merge_logs: true,
        log_date_format: 'YYYY-MM-DD HH:mm Z',
        exec_mode: 'cluster',
        instances: '4',
        source_map_support: false,
        env: {
            NODE_ENV: 'production'
        }
    }
]
};