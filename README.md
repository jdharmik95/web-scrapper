# web-scrapper

### Setting up the project

After cloning the project in local. Please follow below steps to start the application

1. Ensure you have yarn, node installed locally in the system.
2. Run command `yarn` for installing dependencies of project.
3. Run command `yarn start` for starting the web-scrapper application
4. Run command `yarn test` for running testcases.

Once the node server is running in the local, Hit http://localhost:3000/, you should be able to see the home screen of the app.


### Usage

1. On hitting http://localhost:3000/ in browser, you will see home screen.
2. There are available sources and input section for running web-scrapper on new link.
3. In Available sources, you can click on view links to see already, store links.
4. click on delete button for deleting a resource.
5. Enter the url in input section for search a new link
6. Once web-scrapper is successful, it redirects you to /source page where you can see the latest links.
7. Click on `save` button to store the links in database, on saving, it will start appearing in available resources.
8. For fetching latest links,click on `Run scrapper` button in /source page, this will display you the latest links that are parsed on the given source.
9. On clicking save button, it will ovveride / save the source in database.
10. There is `go to sources` link on top of /source page, on clicking that link you can navigate back to the home screen.


### Techincal details

1. Used `Express` (node server), `Firestore` (document database)
2. Written application in `Typescript`.
3. Used `puppeteer` for rendering the source link, and cheerio for parsing the html. 
4. Used puppeteer because there can be few dynamic websites which load html using javascript, puppeteer is a headless chrome browser, which renders the given url like in normal browser environment. There by whole page gets rendered properly, there by we can get all available links in given source.
5. Used `pm2` for serving web application in cluster mode and monitoring, logging and auto-restarts.
6. Used `nodemon` and typescript compiler in watch mode for listening to the changes in file for local development. Run `yarn start-dev` for local development.
7. Used MustacheJS as templating engine.
8. Used `jest` for running testcases. 