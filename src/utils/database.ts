import admin from 'firebase-admin';
import {firestoreCredentials} from '@config/firestore';
export function initializeDB(){
    admin.initializeApp({
        credential: admin.credential.cert(firestoreCredentials)
      });
}