const urlValidatorRegex = /[(http(s)?):\/\/(www\.)?a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/;

export function isValidUrl(url:string | null = null): boolean{
    if(!url){
        return false;
    }
    const result: string[] | null = url.match(urlValidatorRegex);
    if(result){
        return true;
    }
    return false;
}
