const html = `
<html>
    <head>
        <title>Web Scrapper</title>
        <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.7.0/moment.min.js" type="text/javascript"></script>
    </head>
    <body padding-left: 20px; padding-right: 20px;">
        Hi from the webScrapper!
        <br>    
    <h1>Available Sources</h1>
    <table style="width: 50%; padding-right: 20%; table-layout: fixed; border-collapse: collapse; font-family: Open Sans;">
        <thead>
            <th style="width: 33%; word-break: break-word; text-align: left; vertical-align: middle; padding: 15px; border-right: 1px solid #ddd;">Source Url</th>
            <th style="width: 33%; word-break: break-word; text-align: left; vertical-align: middle; padding: 15px; border-right: 1px solid #ddd;">last saved At</th>
            <th style="width: 33%; word-break: break-word; text-align: left; vertical-align: middle; padding: 15px; border-right: 1px solid #ddd;">Links(associations)</th>
            <th style="width: 33%; word-break: break-word; text-align: left; vertical-align: middle; padding: 15px; border-right: 1px solid #ddd;">Action</th>
        </thead>
        <tbody>
        {{#sources}}
        <tr style="height: 50px; border-top: 1px solid #ddd; border-bottom: 1px solid #ddd;">
            <td style="width: 33%; word-break: break-word; text-align: left; vertical-align: middle; padding: 15px; border-right: 1px solid #ddd;"><a href="{{url}}">{{url}}</a></td>
            <td style="width: 33%; word-break: break-word; text-align: left; vertical-align: middle; padding: 15px; border-right: 1px solid #ddd;">{{lastSavedAt}}</td>
            <td style="width: 33%; word-break: break-word; text-align: left; vertical-align: middle; padding: 15px; border-right: 1px solid #ddd;"><a href="{{link}}">View links</a></td>
            <td style="width: 33%; word-break: break-word; text-align: left; vertical-align: middle; padding: 15px; border-right: 1px solid #ddd;"><form action="/delete-source" method="post"><input type="hidden" name="url" value="{{url}}"><input type="submit" name="{{lastSavedAt}}" value="Delete"></form></td>
        </tr>
        {{/sources}}
        </tbody>
    </table>
    <h1> Search a new source</h1>
    <form action="/source" method="get">
        <h3>Enter url: </h3>
        <input type="text" name="url" placeholder="Eg:- https://www.leadsquared.com" style="width:30%">
        <input type="submit" name="search">
        <br><br>
        <div>After clicking submit, please wait, while is web scrapping running, page would be automatically redirected once the scrapping is done.</div>
        <div style="color:red;">{{error}}</div>
        <div style="color:teal;">{{success}}</div>
    </form>
    </body>
</html>
`;
export default html;