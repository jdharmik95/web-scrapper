const html = `
<html>
    <head>
        <title>Web Scrapper</title>
    </head>
    <body padding-left: 20px; padding-right: 20px;">
    {{#source}}
        <h3><a href="/">Go to sources (home)</a><h3>
        
        <h1>URL: {{url}}</h1>

        {{#formattedSavedAt}}
        <br>
            <div>
                <b>last saved at:</b> <span>{{formattedSavedAt}}</span>
            </div>
        <br>
        {{/formattedSavedAt}}
        {{#error}}<h3 style="color:teal;">{{error}}</h3>{{/error}}
        {{#success}}<h3 style="color:teal;">{{success}}</h3>{{/success}}
        <form action="/source" method="post">
            <input type="hidden" name="url" value="{{url}}">
            <b>Save data to datastore:</b> <input type="hidden" name="stringifiedSourceData" value="{{stringifiedSourceData}}">
            <input type="submit" value="Save">
        </form>
        <form action="/source" method="get">
            <input type="hidden"  name="mode" value="latest">
            <input type="hidden" name="url" value="{{url}}">
            <b>Fetch latest available links for given above source URL: </b><input type="submit" value="Run scrapper">
        </form>
        <h2>Links (Displaying only website links)</h2> 
        <table style="width: 100%; table-layout: fixed; border-collapse: collapse; font-family: Open Sans;">
            <thead>
                <tr>
                    <th style="width: 33%; word-break: break-word; text-align: left; vertical-align: middle; padding: 15px; border-right: 1px solid #ddd;">link</th>
                    <th style="width: 33%; word-break: break-word; text-align: left; vertical-align: middle; padding: 15px; border-right: 1px solid #ddd;">isRelative</th>
                    <th style="width: 33%; word-break: break-word; text-align: left; vertical-align: middle; padding: 15px; border-right: 1px solid #ddd;">metadata</th>
                    <th style="width: 33%; word-break: break-word; text-align: left; vertical-align: middle; padding: 15px; border-right: 1px solid #ddd;">caption</th>
                    <th style="width: 33%; word-break: break-word; text-align: left; vertical-align: middle; padding: 15px; border-right: 1px solid #ddd;">open link</th>
                </tr>
            </thead>
            <tbody>
            {{#links}}
                <tr style="height: 50px; border-top: 1px solid #ddd; border-bottom: 1px solid #ddd;">
                    <td style="width: 33%; word-break: break-word; text-align: left; vertical-align: middle; padding: 15px; border-right: 1px solid #ddd;">{{href}}</td>
                    <td style="width: 33%; word-break: break-word; text-align: left; vertical-align: middle; padding: 15px; border-right: 1px solid #ddd;">{{isRelative}}</td>
                    <td style="width: 33%; word-break: break-word; text-align: left; vertical-align: middle; padding: 15px; border-right: 1px solid #ddd;">
                        {{metadata}}
                    </td>
                    <td style="width: 33%; word-break: break-word; text-align: left; vertical-align: middle; padding: 15px; border-right: 1px solid #ddd;">{{caption}}</td>
                    <td style="width: 33%; word-break: break-word; text-align: left; vertical-align: middle; padding: 15px; border-right: 1px solid #ddd;"><a href="{{completeLink}}" target="_blank">Open in newtab</a></td>
                </tr>
            {{/links}}
            </tbody>
        </table>
    {{/source}}
    </body>
</html>
`;
export default html;