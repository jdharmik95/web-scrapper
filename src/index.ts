import express from 'express';
import 'module-alias/register';
import dotenv from 'dotenv';
import { initializeDB } from '@utils/database';
import middlewares from './middlewares';
dotenv.config();

const app:express.Application = express();
app.use(middlewares);


initializeDB();  // initialize firebase NoSQL database connection and using it inside collections

const server = app.listen(3000, () => {  // starting express(node) server
  console.log(`Listening on port 3000`);
});

process.on('SIGINT', async function(err) {
  server.close();
  process.exit(1);
});

process.on('uncaughtException', function(err) {
  server.close();
});