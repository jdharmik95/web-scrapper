import { ISourceData } from "@interfaces/ISourceData";
import { getScrappedLinks } from "./scrapper";
import { addMetadataOfLinks } from "./fetchmetadata";
import { ILinkData } from "@interfaces/ILinkData";

export async function runWebScrapper(url: string): Promise<ISourceData>{

    let links:ILinkData[] = [];
    links = await getScrappedLinks(url);
    links = await addMetadataOfLinks(links);

    // removing hyperlinks which are not websites
    links = links.filter(function(link){
        return link.isWebsite;
    });
    links = links.map(function(link:ILinkData){
        link.metadata = JSON.stringify(link['metadata']);
        return link;
    })
    return {
        url,
        links
    };
}