import cheerio from 'cheerio';
import puppeteer,{Browser,Page} from 'puppeteer';
import { ILinkData } from '@interfaces/ILinkData';
import { getRandomString } from '@utils/utilFunctions';

export async function getScrappedLinks(url:string):Promise<ILinkData[]>{

    const hyperLinks:ILinkData[] = [];
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    await page.setDefaultNavigationTimeout(0);
    await page.goto(url);
    const html = await page.content();
    const $ = cheerio.load(html,{ignoreWhitespace: true });


    $('a').each(function(index:number,element:CheerioElement){
        let caption:string='-';
        let href:string | null = null;
        let validUrl = false;
        let isRelative:boolean = false;
        if(element && element.attribs['href']){
            href = element.attribs['href'];
            caption = element.children && element.children[0] && element.children[0].data ? element.children[0].data:'-';
            caption = caption?.trim();

            // few links may have img tag inside anchor tag to display logo instead of text, then caption would be '-'

            if(href.startsWith('//')){   // few sources wikipedia.com href links starting with //, this is for handling those. 
                isRelative = false;
                validUrl = true;
                href = href.slice(2,href.length);
                href = 'https://'+href;
            }

            if(href.startsWith('/')){ // checking if link starts with / and marking it as relative
                isRelative = true;
                validUrl = true;
            }

            if(href.startsWith('http')){
                validUrl = true;
                isRelative = false;
            }
            if(validUrl){
                hyperLinks.push({
                    id: getRandomString(20),
                    href,
                    isRelative: isRelative,
                    caption,
                    completeLink: isRelative?url+href: href
                });
            }
        }
    });
    return hyperLinks;
}