import {getScrappedLinks} from './scrapper';
import { runWebScrapper } from '@libs/sourceManager';
import { ILinkData } from '@interfaces/ILinkData';
import { ISourceData } from '@interfaces/ISourceData';

const sourceUrl = 'https://www.google.com';

test('get scrapper links', async () => {
    let sourceData:ISourceData = await runWebScrapper(sourceUrl);
    // checking whether the manadtory fields have been returned
    expect(sourceData).toHaveProperty('url');
    expect(sourceData).toHaveProperty('links');

    let links = sourceData.links;
    expect(links.length).toBeGreaterThan(0);
});