import {getScrappedLinks} from './scrapper';
import { ILinkData } from '@interfaces/ILinkData';

const sourceUrl = 'https://www.google.com';

test('get scrapper links', async () => {
    let scrappedLinks:ILinkData[] = await getScrappedLinks(sourceUrl);
    expect(scrappedLinks.length).toBeGreaterThan(0);

    for(let link of scrappedLinks){
        expect(link).toHaveProperty('id');
        expect(link).toHaveProperty('href');
        expect(link).toHaveProperty('isRelative');
        expect(link).toHaveProperty('completeLink');
    }

});