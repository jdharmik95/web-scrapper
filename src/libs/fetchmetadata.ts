import { ILinkData } from "@interfaces/ILinkData";
import {chunk} from '@utils/utilFunctions';
import fetch from 'node-fetch';
import { isValidUrl } from "@utils/validator";
export async function addMetadataOfLinks(links:ILinkData[]): Promise<ILinkData[]>{

    let linksHash:any = {};
    for(let link of links){
        linksHash[link['completeLink']] = link;
    }

    let linksToHit = Object.keys(linksHash);

    let promises:any = [];

    // here for getting the preview of the link, we can use puppeteer instead of fetch api, it provides way for capturing screenshot of loaded page.
    // const snapshot = await page.screenshot({path: 'example.png'});


    for(let link of linksToHit){
        if(isValidUrl(link)){
            promises.push(fetch(link).then(function(response){
                let headersArray:any = [];
                response.headers.forEach(function(val,key){
                    headersArray.push({
                        key,
                        val
                    });
                });
                let contentType:string | null = response.headers.get('content-type');
                if(contentType && contentType.includes('text/html')){ 
                    // checking if response type is text/html to mark as website
                    linksHash[link]['isWebsite'] = true; 
                }else{
                    linksHash[link]['isWebsite'] = false;
                }
                linksHash[link]['metadata'] = {
                    caption: linksHash[link]['caption'],
                    server: response.headers.get('server'),
                    'last-modified': response.headers.get("last-modified"),
                    'content-encoding': response.headers.get("content-encoding"),
                    'content-size': response.headers.get("content-length") 
                    // value would be null if Access-Control-Expose-Headers: Content-Length is not set in given link headers
                }
            }));
        }
    }

    // here I'm making fetch requests in chunks of 10, and used Promise.all() to wait for the responses from each set of 10
    // this is for making concurrent fetch requests in sets of 10

    let chunkedPromises: any[] = chunk(promises,10);

    for(let chunk of chunkedPromises){
        await Promise.all(chunk);
    }
    
    return Object.values(linksHash);
  }

  
