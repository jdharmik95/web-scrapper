import {formatUrl} from './formatter';
test('format - google.com', () => {
    expect(formatUrl('google.com')).toBe('https://www.google.com');
});

test('format - https://google.com', () => {
    expect(formatUrl('https://google.com')).toBe('https://www.google.com');
});

test('format - www.google.com', () => {
    expect(formatUrl('www.google.com')).toBe('https://www.google.com');
});

test('format - www.google.com//', () => {
    expect(formatUrl('www.google.com//')).toBe('https://www.google.com');
});

test('format - www.google.com/', () => {
    expect(formatUrl('www.google.com//')).toBe('https://www.google.com');
});

test('format - www.google', () => {
    expect(formatUrl('www.google')).toBe('https://www.google');
});