export function formatUrl(url: any){
    url = url.replace(/^.*:\/\//i, ''); // remove any http or https://
    url = url.replace('www.',''); // remove any www.
    url = url.replace(/\/+$/, ''); // remove any trailing slashes

    return 'https://www.'+url;
}