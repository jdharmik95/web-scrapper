import {addMetadataOfLinks} from './fetchmetadata';
import { getRandomString } from '@utils/utilFunctions';
import { ILinkData } from '@interfaces/ILinkData';
const mockLinks = [
    {
      "id": getRandomString(20),
      "href":"https://www.leadsquared.com/",
      "isRelative":false,
      "completeLink": "https://www.leadsquared.com/"
    },
    {
      "id": getRandomString(20),
      "href":"https://www.leadsquared.com/crm-software/",
      "isRelative":false,
      "caption": "CRM software",
      "completeLink": "https://www.leadsquared.com/crm-software/"
    }
];

const mockLinksHash:any = {};

for(let link of mockLinks){
    mockLinksHash[link['id']] = link;
}

test('addMetadataOfLinks has modified - check mandatory fields and added metadata field', async () => {
    let returnedLinksData:ILinkData[] = await addMetadataOfLinks(mockLinks);
    for(let link of returnedLinksData){
        
      // check if addMetadataOfLinks has modified fields apart from adding metadata

      expect(link).toHaveProperty('href');
      expect(link).toHaveProperty('isRelative');
      expect(link).toHaveProperty('completeLink');

      // checking if metadata field is added

      expect(link).toHaveProperty('metadata');
      expect(link['metadata']).toBeTruthy();

      // check if addMetadataOfLinks has modified any other fields of ILinkData
      expect(link['href']).toBe(mockLinksHash[link['id']]['href']);
      expect(link['isRelative']).toBe(mockLinksHash[link['id']]['isRelative']);
      expect(link['completeLink']).toBe(mockLinksHash[link['id']]['completeLink']);
    }
});


