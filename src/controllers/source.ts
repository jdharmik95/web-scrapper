import sourceTemplate from '@views/source';
import Mustache from 'mustache';
import { Request, Response, NextFunction, query } from 'express';
import {ILinkData} from '@interfaces/ILinkData';
import { getSources as getSourcseFromDB,syncSource as syncSourceToDB ,getSpecificSource as getSourceFromDB, syncSource, deleteSource as deleteSourceFromDB} from '@collections/source';
import { ISourceData } from '@interfaces/ISourceData';
import { runWebScrapper } from '@libs/sourceManager';
import { formatUrl } from '@libs/formatter';
import moment from 'moment';
import { isValidUrl } from '@utils/validator';

export async function getSource(req:Request,res: Response,next: NextFunction) {
    let queryParams = req.query;
    let links:ILinkData[] = [];
    let success:any = '';
    let error:any = '';
    let mode:any = 'saved'; // two possible value (saved,latest)
    let source:ISourceData | null = null;
    if(queryParams['success']){
        success = queryParams['success'];
    }
    if(queryParams['error']){
        error = queryParams['error'];
    }
    if(queryParams['mode']){
        mode=queryParams['mode'];
    }
    let url:any = queryParams['url'];
    if(!isValidUrl(url)){
        res.redirect('/?error=Invalid url is given, please check source url.');
        return;
    }
    try{
        let url:any = formatUrl(queryParams['url']);
        let sourceFromDB:ISourceData | null = await getSourceFromDB(url);
        let isFromStore:boolean = true;
        source = sourceFromDB;
        if(!sourceFromDB || mode=='latest'){
            source = await runWebScrapper(url);
            isFromStore = false;
            success= 'Fetched the latest links for given source url, please click save to override/save the links in datastore.';
        }
        if(source){
            source['lastSavedAt'] = sourceFromDB && sourceFromDB['lastSavedAt']?sourceFromDB['lastSavedAt']:null;
            if(source['lastSavedAt']){
                source['formattedSavedAt'] = moment.unix(source['lastSavedAt']).format('LLLL');
            }
        }
        const renderedHtml = Mustache.render(sourceTemplate,{
            success,
            source,
            isFromStore,
            stringifiedSourceData: JSON.stringify(source)
        });
        // res.json(links);
        res.send(renderedHtml);
    }catch(err){
        error = err;
        res.redirect(`/?error=${error}`);
    }
}

export async function saveSource(req:Request,res: Response,next: NextFunction){
    let reqestBody = req.body;
    let success:string = '';
    let error:string = '';
    if(!isValidUrl(reqestBody['url'])){
        res.redirect('/?error=Invalid url is given, please check source url.');
        return;
    }
    let url:string = formatUrl(reqestBody['url']);
    try{
        let source:ISourceData | null =  JSON.parse(reqestBody.stringifiedSourceData);
        if(source){
            await syncSource(source.url,source.links);
            success = 'Saved the source successfully.';
        }
    }catch(err){
        console.log('error is ');
        console.log(err);
        error = err;
    }
    res.redirect(`/source?url=${url}&error=${error}&success=${success}`);
}


export async function deleteSource(req:Request,res: Response,next: NextFunction){
    let success:string='';
    let error:string='';
    try{
        let url = req.body.url;
        console.log('inside deleteSource url is',url);
        await deleteSourceFromDB(url);
        success = 'Deleted the source successfully.';
    }catch(err){
        console.log('error is ');
        console.log(err);
        error = err;
    }
    return res.redirect(`/?error=${error}&sucess=${success}`);
}