import homeTemplate from '@views/home';
const Mustache = require('mustache');
import { Request, Response, NextFunction } from 'express';
import { getSources } from '@collections/source';
import moment from 'moment';


export async function displayHome(req:Request,res: Response,next: NextFunction) {
    let sources = await getSources();
    sources = sources.map(function(source:any){
        source['link'] = `/source?url=${source.url}`;
        source['lastSavedAt'] = source['lastSavedAt']?moment.unix(source['lastSavedAt']).format('LLLL'): null;
        return source;
    });
    const renderedHtml = Mustache.render(homeTemplate,{
        sources,
        error: req.query?.error,
        success: req.query?.success
    });
    res.send(renderedHtml);
};