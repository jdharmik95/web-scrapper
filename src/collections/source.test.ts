import { ISourceData } from "@interfaces/ISourceData";
import { runWebScrapper } from "@libs/sourceManager";
import { syncSource, getSpecificSource, getSources, deleteSource } from "./source";
import { initializeDB } from "@utils/database";

initializeDB();
const sourceUrl = 'https://xkcd.com';

test('fetch and sync source to database', async() => {
    let source: ISourceData = await runWebScrapper(sourceUrl);
    let response = await syncSource(source.url,source.links);
    expect(response).not.toBeNull();
});

test('get specific source from database', async() => {
    let response = await getSpecificSource(sourceUrl);
    expect(response).toHaveProperty('url');
    expect(response).toHaveProperty('lastSavedAt');
    expect(response).toHaveProperty('links');
});

test('get all sources', async() => {
    let sources:ISourceData[] = await getSources();

    for(let source of sources){
        expect(source).toHaveProperty('url');
        expect(source).toHaveProperty('lastSavedAt');
        expect(source).toHaveProperty('links');
    }
});

test('delete specific source in database', async() => {
    let response = await deleteSource(sourceUrl);
    expect(response).not.toBeNull();
});