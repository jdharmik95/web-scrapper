import { ILinkData } from "@interfaces/ILinkData";
import moment from "moment";
import admin from 'firebase-admin';
import { ISourceData } from "@interfaces/ISourceData";
const { base64encode } = require('nodejs-base64');

export async function syncSource(url:string,links:ILinkData[]): Promise<FirebaseFirestore.WriteResult> {  
    const db = admin.firestore();
    // this is a sync func, which updates the existing doc of that domain, and inserts if doc is not present.
    let base64EncodedDomain = base64encode(url);
    let response = await db.collection('sources').doc(base64EncodedDomain).set({
        url,
        links,
        lastSavedAt: moment().unix()
    });
    return response;
}

export async function getSpecificSource(url:string): Promise<ISourceData | null>{
    const db = admin.firestore();
    let base64EncodedDomain = base64encode(url);
    let snapshot = await db.collection('sources').doc(base64EncodedDomain).get();
    let snapshotData: any | undefined = snapshot.data();
    if(snapshotData){
        return {
            url: snapshotData.url,
            lastSavedAt: snapshotData.lastSavedAt,
            links: snapshotData.links
        };
    }
    return null;
}

export async function getSources(): Promise<ISourceData[]>{
    const db = admin.firestore();
    let result:any = {};
    let snapshot = await db.collection('sources').get();
    snapshot.forEach((doc) => {
        result[doc.id] = doc.data()
    });
    return Object.values(result);
}


export async function deleteSource(url:string): Promise<FirebaseFirestore.WriteResult>{
    const db = admin.firestore();
    let base64EncodedDomain = base64encode(url);
    return await db.collection('sources').doc(base64EncodedDomain).delete();
}

// export async function deleteLinkFromSource(url:string='',linkId: string=''){
//     let base64EncodedDomain = base64encode(url);
//     await db.collection('sources').doc(base64EncodedDomain).update({
//         links: links.filter(link => link.id !== linkId)
//       });
// }