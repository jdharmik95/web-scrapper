import { Request, Response, NextFunction, RequestHandler } from "express";
import webRoutes from './webRoutes';
import apiRoutes from './apiRoutes';
import {compose} from 'compose-middleware';

const arrayOfMiddlewares:any[] = [
    function(req:Request,res:Response,next:NextFunction){
        console.log('pre middleware for routes13 - middleware');
        next();
    },
    webRoutes,
    apiRoutes,
    function (req:Request,res:Response,next:NextFunction) {
        res.status(404).send("Sorry can't find that!")
        next()
    }
];

export default  compose(arrayOfMiddlewares);