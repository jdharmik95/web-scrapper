const compose = require('compose-middleware').compose;
import routesMiddleware from './routes';
import errorMiddleware from './error';
const composedMiddleware = compose([
    routesMiddleware,
    errorMiddleware
]);

export default composedMiddleware;