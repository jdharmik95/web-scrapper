import {ILinkData} from './ILinkData';
export interface ISourceData{
    url: string,
    links: ILinkData[],
    lastSavedAt?: number | null,
    formattedSavedAt?: string | null
}

