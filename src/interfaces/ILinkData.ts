import {ILinkMetaData} from './ILinkMetaData';
export interface ILinkData{
    isRelative: boolean,
    id: string,
    href: string,
    caption?: string,
    completeLink: string,
    isWebsite?: boolean,
    metadata?: ILinkMetaData | string
}

// interfaces are defining custom types in typescript, above is custom type for ILinkData