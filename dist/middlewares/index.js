"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var compose = require('compose-middleware').compose;
var routes_1 = __importDefault(require("./routes"));
var error_1 = __importDefault(require("./error"));
var composedMiddleware = compose([
    routes_1.default,
    error_1.default
]);
exports.default = composedMiddleware;
