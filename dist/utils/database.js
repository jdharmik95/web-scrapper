"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.initializeDB = void 0;
var firebase_admin_1 = __importDefault(require("firebase-admin"));
var firestore_1 = require("@config/firestore");
function initializeDB() {
    firebase_admin_1.default.initializeApp({
        credential: firebase_admin_1.default.credential.cert(firestore_1.firestoreCredentials)
    });
}
exports.initializeDB = initializeDB;
