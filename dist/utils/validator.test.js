"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var validator_1 = require("./validator");
// true condition checks
test('validate url - leadsquared.com', function () {
    expect(validator_1.isValidUrl('leadsquared.com')).toBe(true);
});
test('validate url - https://leadsquared.com', function () {
    expect(validator_1.isValidUrl('https://leadsquared.com')).toBe(true);
});
test('validate url - www.leadsquared.com', function () {
    expect(validator_1.isValidUrl('www.leadsquared.com')).toBe(true);
});
test('validate url - www.leadsquared.com//', function () {
    expect(validator_1.isValidUrl('www.leadsquared.com//')).toBe(true);
});
test('validate url - www.leadsquared.com/', function () {
    expect(validator_1.isValidUrl('www.leadsquared.com//')).toBe(true);
});
// false condition checks
test('validate url - www.leadsquared', function () {
    expect(validator_1.isValidUrl('www.leadsquared')).toBe(false);
});
test('validate url - www/leadsquared', function () {
    expect(validator_1.isValidUrl('www/leadsquared')).toBe(false);
});
test('validate url - leadsquared', function () {
    expect(validator_1.isValidUrl('leadsquared')).toBe(false);
});
test('validate url - www.$leadsquared', function () {
    expect(validator_1.isValidUrl('www/leadsquared')).toBe(false);
});
test('validate url - //', function () {
    expect(validator_1.isValidUrl('www/leadsquared')).toBe(false);
});
