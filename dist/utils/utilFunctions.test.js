"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var utilFunctions_1 = require("./utilFunctions");
var listOfNumbers = [1, 2, 3, 4, 5, 6];
test('[1,2,3,5,6] in chunks of 2', function () {
    var result = utilFunctions_1.chunk(listOfNumbers, 2);
    expect(result.length).toBe(3);
    expect(result[0]).toHaveLength(2);
    expect(result[1]).toHaveLength(2);
    expect(result[2]).toHaveLength(2);
});
test('[] in chunks of 2', function () {
    expect(utilFunctions_1.chunk([], 2)).toStrictEqual([]);
});
