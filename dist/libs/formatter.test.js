"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var formatter_1 = require("./formatter");
test('format - google.com', function () {
    expect(formatter_1.formatUrl('google.com')).toBe('https://www.google.com');
});
test('format - https://google.com', function () {
    expect(formatter_1.formatUrl('https://google.com')).toBe('https://www.google.com');
});
test('format - www.google.com', function () {
    expect(formatter_1.formatUrl('www.google.com')).toBe('https://www.google.com');
});
test('format - www.google.com//', function () {
    expect(formatter_1.formatUrl('www.google.com//')).toBe('https://www.google.com');
});
test('format - www.google.com/', function () {
    expect(formatter_1.formatUrl('www.google.com//')).toBe('https://www.google.com');
});
test('format - www.google', function () {
    expect(formatter_1.formatUrl('www.google')).toBe('https://www.google');
});
