"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.formatUrl = void 0;
function formatUrl(url) {
    url = url.replace(/^.*:\/\//i, ''); // remove any http or https://
    url = url.replace('www.', ''); // remove any www.
    url = url.replace(/\/+$/, ''); // remove any trailing slashes
    return 'https://www.' + url;
}
exports.formatUrl = formatUrl;
