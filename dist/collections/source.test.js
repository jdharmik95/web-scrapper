"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var sourceManager_1 = require("@libs/sourceManager");
var source_1 = require("./source");
var database_1 = require("@utils/database");
database_1.initializeDB();
var sourceUrl = 'https://xkcd.com';
test('fetch and sync source to database', function () { return __awaiter(void 0, void 0, void 0, function () {
    var source, response;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, sourceManager_1.runWebScrapper(sourceUrl)];
            case 1:
                source = _a.sent();
                return [4 /*yield*/, source_1.syncSource(source.url, source.links)];
            case 2:
                response = _a.sent();
                expect(response).not.toBeNull();
                return [2 /*return*/];
        }
    });
}); });
test('get specific source from database', function () { return __awaiter(void 0, void 0, void 0, function () {
    var response;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, source_1.getSpecificSource(sourceUrl)];
            case 1:
                response = _a.sent();
                expect(response).toHaveProperty('url');
                expect(response).toHaveProperty('lastSavedAt');
                expect(response).toHaveProperty('links');
                return [2 /*return*/];
        }
    });
}); });
test('get all sources', function () { return __awaiter(void 0, void 0, void 0, function () {
    var sources, _i, sources_1, source;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, source_1.getSources()];
            case 1:
                sources = _a.sent();
                for (_i = 0, sources_1 = sources; _i < sources_1.length; _i++) {
                    source = sources_1[_i];
                    expect(source).toHaveProperty('url');
                    expect(source).toHaveProperty('lastSavedAt');
                    expect(source).toHaveProperty('links');
                }
                return [2 /*return*/];
        }
    });
}); });
test('delete specific source in database', function () { return __awaiter(void 0, void 0, void 0, function () {
    var response;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, source_1.deleteSource(sourceUrl)];
            case 1:
                response = _a.sent();
                expect(response).not.toBeNull();
                return [2 /*return*/];
        }
    });
}); });
