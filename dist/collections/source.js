"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteSource = exports.getSources = exports.getSpecificSource = exports.syncSource = void 0;
var moment_1 = __importDefault(require("moment"));
var firebase_admin_1 = __importDefault(require("firebase-admin"));
var base64encode = require('nodejs-base64').base64encode;
function syncSource(url, links) {
    return __awaiter(this, void 0, void 0, function () {
        var db, base64EncodedDomain, response;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    db = firebase_admin_1.default.firestore();
                    base64EncodedDomain = base64encode(url);
                    return [4 /*yield*/, db.collection('sources').doc(base64EncodedDomain).set({
                            url: url,
                            links: links,
                            lastSavedAt: moment_1.default().unix()
                        })];
                case 1:
                    response = _a.sent();
                    return [2 /*return*/, response];
            }
        });
    });
}
exports.syncSource = syncSource;
function getSpecificSource(url) {
    return __awaiter(this, void 0, void 0, function () {
        var db, base64EncodedDomain, snapshot, snapshotData;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    db = firebase_admin_1.default.firestore();
                    base64EncodedDomain = base64encode(url);
                    return [4 /*yield*/, db.collection('sources').doc(base64EncodedDomain).get()];
                case 1:
                    snapshot = _a.sent();
                    snapshotData = snapshot.data();
                    if (snapshotData) {
                        return [2 /*return*/, {
                                url: snapshotData.url,
                                lastSavedAt: snapshotData.lastSavedAt,
                                links: snapshotData.links
                            }];
                    }
                    return [2 /*return*/, null];
            }
        });
    });
}
exports.getSpecificSource = getSpecificSource;
function getSources() {
    return __awaiter(this, void 0, void 0, function () {
        var db, result, snapshot;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    db = firebase_admin_1.default.firestore();
                    result = {};
                    return [4 /*yield*/, db.collection('sources').get()];
                case 1:
                    snapshot = _a.sent();
                    snapshot.forEach(function (doc) {
                        result[doc.id] = doc.data();
                    });
                    return [2 /*return*/, Object.values(result)];
            }
        });
    });
}
exports.getSources = getSources;
function deleteSource(url) {
    return __awaiter(this, void 0, void 0, function () {
        var db, base64EncodedDomain;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    db = firebase_admin_1.default.firestore();
                    base64EncodedDomain = base64encode(url);
                    return [4 /*yield*/, db.collection('sources').doc(base64EncodedDomain).delete()];
                case 1: return [2 /*return*/, _a.sent()];
            }
        });
    });
}
exports.deleteSource = deleteSource;
// export async function deleteLinkFromSource(url:string='',linkId: string=''){
//     let base64EncodedDomain = base64encode(url);
//     await db.collection('sources').doc(base64EncodedDomain).update({
//         links: links.filter(link => link.id !== linkId)
//       });
// }
